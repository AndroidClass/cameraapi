package com.hmasrour.cameraapli;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imageView;
    Button get_photo;
    Button save;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindWidgets();

        get_photo.setOnClickListener(this);

    }



    public void getPermission() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("PLAYGROUND", "Permission is not granted, requesting");
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);
        } else {
            Log.d("PLAYGROUND", "Permission is granted");
        }
    }

    public void bindWidgets() {
        imageView = findViewById(R.id.imageView);
        get_photo = findViewById(R.id.get_photo);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.get_photo) {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                Log.d("Camera", "Permission is not granted, requesting");
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 123);
            }
            Intent photo = new Intent("android.media.action.IMAGE_CAPTURE");
            startActivityForResult(photo, 100);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (requestCode) {
                case 100:
                    if (resultCode == RESULT_OK) {
                        Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
                        imageView.setImageBitmap(imageBitmap);
                    } else if (resultCode == RESULT_CANCELED) {
                        Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
                    }
                    break;

            }
        } catch (Exception e)

        {
            Log.d("onActivityResult", "onActivityResult: " + e.toString());
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
        }
    }
}
